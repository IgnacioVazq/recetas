<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Ignacio',
            'email' => 'correo@correo.com',
            'password' => Hash::make('12345678'),
            'url' => 'https://ignacio.com',
        ]);
        // $user->perfil()->create(); Conflicto, esta linea crea otro perfil (dos perfiles por usuario)
        
        $user2 = User::create([
            'name' => 'Jorge',
            'email' => 'correo2@correo.com',
            'password' => Hash::make('12345678'),
            'url' => 'https://jorge.com',
        ]);
        // $user2->perfil()->create(); 

        $user3 = User::create([
            'name' => 'Mauricio',
            'email' => 'correo3@correo.com',
            'password' => Hash::make('12345678'),
            'url' => 'https://mauricio.com',
        ]);
        // $user3->perfil()->create();

    }
}
