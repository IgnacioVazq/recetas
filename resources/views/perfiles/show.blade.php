{{-- vista perfil --}}
@extends('layouts.app')

@section('botones')
    <div class="py-4 mt-4 col-12">
        <a href="{{ route('recetas.index')  }}" class="btn btn-outline-primary mr-2 text-uppercase font-weight-bold"> 
            <svg class="w-6 h-6 icono" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 15l-3-3m0 0l3-3m-3 3h8M3 12a9 9 0 1118 0 9 9 0 01-18 0z"></path></svg>
            Regresar 
        </a>
    </div>
@endsection

@section('content')
    
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                @if ( $perfil->imagen )
                    <img src="/storage/{{ $perfil->imagen }}" class="w-100 rounded-circle" alt="Imagen chef">
                @endif
            </div>
            <div class="col-md-7 mt-5 mt-md-0">
                <h2 class="text-center mb-2 text-primary"> {{$perfil->usuario->name}} </h2>
                <a href="{{ $perfil->usuario->url }}"> Visitar Sitio Web </a>
                {{-- {{$perfil->usuario}} --}}
                <div class="biografia">
                    {!! $perfil->biografia !!}  {{-- Con esta sintaxis toma el contenido de la  biografia mostrando el formato del texto enriquecido --}}
                    {{-- {{ $perfil->biografia }}    Con esta sintaxis retorna el contendo de biogrfia como texto plano, no le da formato de texto enriquecido --}}
                </div>
            </div>

        </div>
    </div>

    {{-- {{ $recetas }} --}}
    <h2 class="text-center my-5">Recetas creadas por:{{ $perfil->usuario->name}}</h2>
    <div class="container">
        <div class="row mx-auto bg-white p-4">
            @if( count($recetas) > 0 )
                @foreach ($recetas as $receta)
                    <div class="col-md-4 mb-4">
                        <div class="card">
                            <img src="/storage/{{$receta->imagen}}" class="card-img-top" alt="Imagen receta">
                            <div class="card-body">
                                <h3>{{ $receta->titulo }}</h3>
                                <a href="{{ route('recetas.show', ['recetas' => $receta->id]) }}" class="btn btn-primary d-block mt-4 text-uppercase font-weight-bold"> Ver receta</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else 
                <p class="text-center w-100">No hay recetas aún</p>
            @endif
        </div>
        <div class="d-flex justify-content-center">
            {{ $recetas->links() }}
        </div>
    </div>


@endsection