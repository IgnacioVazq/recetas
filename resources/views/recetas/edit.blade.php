@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.min.css" integrity="sha512-5m1IeUDKtuFGvfgz32VVD0Jd/ySGX7xdLxhqemTmThxHdgqlgPdupWoSN8ThtUSLpAGBvA8DY2oO7jJCrGdxoA==" crossorigin="anonymous" />
@endsection

@section('botones')
    <div class="py-4 mt-5 col-12">
        <a href="{{ route('recetas.index')  }}" class="btn btn-primary mr-2 text-white"> Regresar </a>
    </div>
@endsection

@section('content')

<h2 class="text-center mb-5"> Editar receta: {{ $recetas->titulo }}  </h2>

<div class="row justify-content-center mt-5">
    <div class="col-md-8">
        <form method="POST" action="{{ route('recetas.update', ['recetas' => $recetas->id]) }}" enctype="multipart/form-data" novalidate> <!-- novalidate para validaciones en Laravel -->
            @csrf

            @method('PUT')
            <div class="form-group">
                <label for="titulo"> Título receta </label>
                <input type="text" name="titulo" class="form-control @error('titulo') is-invalid @enderror" id="titulo" placeholder="Titulo Receta" value="{{ $recetas->titulo }}">

                @error('titulo')
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span> 
                @enderror
            </div>

            <div class="form-group">
                <label for="categoria">Categoria</label>
                <select name="categoria" class="form-control @error('categoria') is-invalid @enderror" id="categoria">
                    <option value="">-- Seleccione --</option>
                    @foreach ($categorias as $categoria)
                        
                    <option value="{{ $categoria->id }}" {{ $recetas->categoria_id == $categoria->id ? 'selected' : '' }}> {{ $categoria->nombre }}</option>

                    @endforeach
                </select>

                @error('categoria')
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span> 
                @enderror
            </div>


            <div class="form-group mt-3">
                <label for="preparacion"> Preparación </label>
                <input id="preparacion" type="hidden" name="preparacion" value="{{ $recetas->preparacion }}">
                <trix-editor input="preparacion" class="form-control @error('preparacion') is-invalid @enderror"></trix-editor>

                @error('preparacion')   
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span> 
                @enderror
            </div>

            <div class="form-group mt-3">
                <label for="ingredientes"> Ingredientes </label>
                <input id="ingredientes" type="hidden" name="ingredientes" value="{{ $recetas->ingredientes }}">
                <trix-editor input="ingredientes" class="form-control @error('ingredientes') is-invalid @enderror"></trix-editor>

                @error('ingredientes')
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span> 
                @enderror
            </div>

            <div class="form-group mt-3">
                <label for="imagen"> Elige la imagen </label>
                <input id="imagen" type="file" class="form-control" name="imagen">

                <div class="mt_4">
                    <p> Imagen Actual: </p>
                    <img src="/storage/{{ $recetas->imagen }}" style="width: 300px">
                </div>

                @error('imagen')
                    <span class="invalid-feedback d-block @error ('imagen') is-invalid @enderror" role="alert">
                        <strong>{{ $message }}</strong>
                    </span> 
                @enderror
            </div>
            
            <div class="from-group">
                <input type="submit" class="btn btn-primary" value="Guardar cambios">
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.min.js" 
    integrity="sha512-2RLMQRNr+D47nbLnsbEqtEmgKy67OSCpWJjJM394czt99xj3jJJJBQ43K7lJpfYAYtvekeyzqfZTx2mqoDh7vg==" 
    crossorigin="anonymous" defer></script>
@endsection